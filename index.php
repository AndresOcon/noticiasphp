<?php
  require('includes/conexion.php');
?>

<!-- Recojo la 'página que quiero cargar' -->
<?php
if(isset($_GET['p'])){
  $p=$_GET['p'];
}else{
  $p='inicio.php'; //Página INICIAL
}

?>



<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Tiendaphp2018</title>
 
    <!-- CSS de Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
    <link href="css/bootstrap-theme.min.css" rel="stylesheet" media="screen">
   
    
 
    <!-- librerías opcionales que activan el soporte de HTML5 para IE8 -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  
  <style>
    
    footer{ text-align: center; }

    li{ list-style: none; }
    nav a { color: white; }

    .encabezado img{ width: 100%; }
    .prod{
      margin-top: 10px;
      padding-bottom: 8px;
      color: green;
    }
    .prod a{
      color:rgba(22,21,20,0.9);
      text-decoration: none;
    }
    .prod a:hover{ color:rgba(22,21,20,1); }
    .prod:hover{
      box-shadow: 1px 1px 2px 1px #999;

    }

    .precio p{ display: inline-block; }

  </style>

  </head>

  <body >

      <section class="container">
      <!-- <section class="encabezado"> -->

        <header>
          <?php include('includes/encabezado.php') ?>
        </header>
       
        <nav>
          <?php include('includes/menu.php') ?>
        </nav>
       <!-- </section> -->

        
        <main>
            <?php include('paginas/'.$p); ?>
          
        </main>

        <footer>
          <?php include('includes/pie.php') ?>
        </footer>
        

      </section>
  






 
    <!-- Librería jQuery requerida por los plugins de JavaScript -->
    <script src="js/jquery-3.3.1.min.js"></script>
 
    <!-- Todos los plugins JavaScript de Bootstrap (también puedes
         incluir archivos JavaScript individuales de los únicos
         plugins que utilices) -->
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>

 <!--  Desconectar de la DB -->
<?php $conexion->close(); ?>
