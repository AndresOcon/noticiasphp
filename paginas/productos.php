
<h3>

	Estás en el listado de productos

	<small>
		<a href="index.php?p=insertar.php">Insertar producto</a>
	</small>

</h3>

<section class="row">

<?php

//Conectar a la DB


//Establecer la pregunta que quiero hacerle (lenguaje SQL)

	$sql="SELECT * FROM productos";


//Ejecuto la pregunta o consulta 
$consulta=$conexion->query($sql);

if($consulta->num_rows==0){
	echo 'No se han encontrado productos';
}

//Procesamos los resultados de la consulta
while($registro=$consulta->fetch_array()){
	?>
		<article class="col-sm-4 col-md-4 prod">
			<header>
				<h4>
					<a href="index.php?p=detalle.php&idProd=<?php echo $registro['idProd'];?>">
						<?php echo $registro['nombreProd']; ?>
					</a>
					<a href="index.php?p=modificar.php&idProd=<?php echo $registro['idProd'];?>">
						<small><span class="glyphicon glyphicon-pencil" style="color: navy;"></span></small>
					</a>
					<a href="index.php?p=borrar.php&idProd=<?php echo $registro['idProd'];?>">
						<small><span class="glyphicon glyphicon-trash" style="color: red;"></span></small>
					</a>
					
				</h4>
				<small class="precio">
					<p>
						<?php echo $registro['precioProd']; ?>
					</p>
					<p>€</p>
				</small>
			</header>
			<section>
				
				<a href="index.php?p=detalle.php&idProd=<?php echo $registro['idProd'];?>">
					<button class="btn">Ver</button>
				</a>

			</section>
		</article>

	<?php
}


?>
</section>